 
import sqlite3
import logging
import csv
from copy import copy
import unicodedata
import sys
import os.path
import os

from datetime import datetime

logging_path=('./logging/') #emplacement des loggings
bdd_path=('./siv.sqlite3') #emplacement de la Base de donnée
csv_path=('./auto.csv') #emplacement de auto.csv

def erreurConnexionBDD(erreur):
    '''
    En cas d'erreur dans la base de donnée, cette fonction est appelée.
    elle la remet à son état d'origine et la ferme. 
    '''
    connection.rollback()
    connection.close()
    logging.warning("Erreur ! réinitialisation de la base de donnée, le programme s'arete" + erreur)

def nettoyage(row):
    '''
    Si le caractère n'est pas voulu alors cette fonction va la delete
    IN: la ligne à nettoyer
    Out: la nouvelle ligne nettoyée 
    '''
    newrow = row.replace(";"," ").replace("--", " ")
    newrow = unicodedata.normalize('NFKD', newrow).encode('ascii', 'ignore')
    return newrow.decode('ascii')

def basededonneedico(row):
    '''
    dictionnaire de donnée du fichier auto.csv qui va s'envoyer dans la base de donnée
    '''
    logging.debug("nettoyage des variables du dictionnaire")
    dicoBDD = {
            'adresse_titulaire': nettoyage(row['adresse_titulaire']),
            'nom': nettoyage(row['nom']),
            'prenom':nettoyage(row['prenom']),
            #cette colonne sera notre clé primaire pour notre table car chaque immatriculation est différente
            'immatriculation':nettoyage(row['immatriculation']), 
            'date_immatriculation':nettoyage(row['date_immatriculation']),
            'vin':nettoyage(row['vin']),
            'marque': nettoyage(row['marque']),
            'denomination_commerciale': nettoyage(row['denomination_commerciale']),
            'couleur': nettoyage(row['couleur']),
            'carrosserie': nettoyage(row['carrosserie']),
            'categorie': nettoyage(row['categorie']),
            'cylindre':nettoyage( row['cylindre']),
            'energie': nettoyage(row['energie']),
            'places': nettoyage(row['places']),
            'poids': nettoyage(row['poids']),
            'puissances': nettoyage(row['puissances']),
            'type': nettoyage(row['type']),
            'variante': nettoyage(row['variante']),
            'version': nettoyage(row['version']),
        }
    return dicoBDD 


def insertiondonnee():
     cursor.execute('''INSERT into caracteristiques (adresse_titulaire, nom, prenom, immatriculation, 
                    date_immatriculation, vin, marque, denomination_commerciale, couleur,
                    carrosserie, categorie, cylindre, energie, places, poids, puissances, 
                    type, variante, version) values(:adresse_titulaire, :nom, :prenom, :immatriculation, 
                    :date_immatriculation, :vin, :marque, :denomination_commerciale, :couleur,
                    :carrosserie, :categorie, :cylindre, :energie, :places, :poids, :puissances, 
                    :type, :variante, :version)''', basededonneedico(row))

def creationTable():
    '''
    Fonction de création de notre base de donnée en insérant nos données.
    '''
    try:
        logging.debug("Creation de la table caracteristiques")
        connection = sqlite3.connect(bdd_path)
        cursor = connection.cursor()
        cursor.execute('''CREATE TABLE caracteristiques (
        adresse_titulaire TEXT,
        nom TEXT,
        prenom TEXT,
        immatriculation TEXT,
        date_immatriculation TEXT,
        vin TEXT,
        marque TEXT,
        denomination_commerciale TEXT,
        couleur TEXT,
        carrosserie TEXT,
        categorie TEXT,
        cylindre TEXT,
        energie TEXT,
        places TEXT,
        poids TEXT,
        puissances TEXT,
        type TEXT,
        variante TEXT,
        version TEXT
        );
        ''')
        connection.commit()
        return cursor, connection
    except sqlite3.Error as identifier:
        erreurConnexionBDD(identifier)

if os.path.exists(logging_path): #Test s'il existe
    if os.path.isdir(logging_path): #test si c'est un dossier
        file_name = (logging_path  + "filelog" +'.log')
    else: #sinon on arrete tout
        print("Erreur log est un fichier")
        exit(1)
else: #sinon on la crée
    try:
        os.mkdir(logging_path)
        file_name = (logging_path + "filelog" +'.log')
    except OSError as error:
            print(error.__cause__)
            exit(1)

if os.path.exists(bdd_path): #test si le fichier de base de donnée existe
    try:
        connection = sqlite3.connect(bdd_path)
        cursor =connection.cursor()
    except sqlite3.Error as identifier:
        erreurConnexionBDD(identifier)
else: #sinon on crée la base de donné et la table
    cursor,connection=creationTable()

logging.basicConfig(format ='%(asctime)s : %(levelname)s : import.py : %(message)s',filename=file_name,level=logging.DEBUG) #configuration du module logging
logging.info("Début du programme") #Début de programme

caracteres_combinaison = dict.fromkeys(c for c in range(sys.maxunicode)
if unicodedata.combining(chr(c)))
logging.info("Fin initialisation du programme") #Fin d'initialisation du programme

if __name__ == '__main__' :
    with open(csv_path) as csvfile:
        logging.info("Ouverture !")
        reader = csv.DictReader(csvfile, delimiter=';')
        for row in reader:
            boucle = 0
            for col in cursor.fetchall():
                boucle = boucle + 1
            if boucle > 1:
                logging.info("boucle n°%d", boucle)
                erreurConnexionBDD("Une ligne est presente plus d'une fois")
                
            else:
                #insertion des données dans caracteristiques
                insertiondonnee()
                logging.info("Ecriture dans la base de données." )
    connection.commit()
    logging.debug("fin !")
    connection.close()
    logging.debug("Base de donnée fermée")
