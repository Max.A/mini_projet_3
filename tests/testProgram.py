import unittest
import os
import csv
import logging
import sqlite3
from datetime import datetime
from program import *

class TestStockageSIV(unittest.TestCase):

    
    def test_creationTable(self):
        self.con.commit()

    def test_nettoyage(self):
            nettoyage(self.list_ligne[0], self.cursor)
            self.con.commit()
            for row in self.cursor.execute('''SELECT nom FROM siv WHERE immatriculation = 'OVC-568' '''):
                data = row[0]
            self.assertEqual(data, 'Smith')

            nettoyage(self.list_ligne[3], self.cursor)
            self.con.commit()
            for row in self.cursor.execute('''SELECT nom FROM siv WHERE immatriculation = 'OVC-568' '''):
                data = row[0]
            self.assertEqual(data, 'Jhon')
            
            nettoyage(self.list_ligne[4], self.cursor)
            self.con.commit()
            for row in self.cursor.execute('''SELECT prenom FROM siv WHERE immatriculation = 'OVC-568' '''):
                data = row[0]
            self.assertEqual(data, 'Jerome')
            for row in self.cursor.execute('''SELECT date_immatriculation FROM siv WHERE immatriculation = 'OVC-568' '''):
                data = row[0]
            self.assertEqual(data, '03/05/2012')
